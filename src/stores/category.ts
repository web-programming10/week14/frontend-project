import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Category from "@/types/Category";
import categoryService from "@/services/category";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useCategoryStore = defineStore("Category", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const categories = ref<Category[]>([]);
  const editedCategory = ref<Category>({ name: "" });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedCategory.value = { name: "" };
    }
  });
  async function getCategories() {
    loadingStore.isLoading = true;
    try {
      const res = await categoryService.getCategories();
      categories.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Category ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveCategory() {
    loadingStore.isLoading = true;
    try {
      if (editedCategory.value.id) {
        const res = await categoryService.updateCategory(
          editedCategory.value.id,
          editedCategory.value
        );
      } else {
        const res = await categoryService.saveCategory(editedCategory.value);
      }

      dialog.value = false;
      await getCategories();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Category ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteCategory(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await categoryService.deleteCategory(id);
      await getCategories();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Category ได้");
    }
    loadingStore.isLoading = false;
  }
  function editCategory(category: Category) {
    editedCategory.value = JSON.parse(JSON.stringify(category));
    dialog.value = true;
  }
  return {
    categories,
    getCategories,
    dialog,
    editedCategory,
    saveCategory,
    editCategory,
    deleteCategory,
  };
});
